```
AH=$GH/reactjs_webapp/  # GH aka GIT_CLONED_HOME

# run 0th
cd $AH
    npx create-react-app .
    npm start

# 2nd run
cd $AH
    npm install ; npm start

# run w/ yarn faster
npm i -g yarn 
cd $AH
    yarn install ; yarn start 

# add routed 0th  ref. https://www.w3schools.com/react/react_router.asp
cd $AH
    yarn add -D react-router-dom
    yarn start
```

TODO implement subpage detail
