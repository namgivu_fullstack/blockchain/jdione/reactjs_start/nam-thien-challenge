import React from 'react'
import ReactDOM from 'react-dom/client'
import {BrowserRouter, Route, Routes} from "react-router-dom"
//
import Layout from  "./page/_Layout"
import NoPage from  "./page/_NoPage"
//
import Home from    "./page/Home"
import KeyPair from "./page/KeyPair"
import Signature from "./page/Signature"
import Transaction from "./page/Transaction"
import Blockchain from "./page/Blockchain"


const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />} >
          <Route index    element={<Home />} />
          <Route path="*" element={<NoPage />} />

          <Route path="blockchain/public-private-keys/keys"        element={<KeyPair />} />
          <Route path="blockchain/public-private-keys/signatures"  element={<Signature />} />
          <Route path="blockchain/public-private-keys/transaction" element={<Transaction />} />
          <Route path="blockchain/public-private-keys/blockchain"  element={<Blockchain />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </>
)
