import TopNav from "./_TopNav"

const KeyPair = () => {
  return (
    <>
      <TopNav activeName='key' />

      <div className="container">
            <div className="card">
                <h4 className="card-header">Public / Private Key Pairs</h4>
                <div className="card-body">
                    <form className="form-horizontal">
                        <div className="form-group"><label className="control-label" htmlFor="data">Private Key</label>
                            <div className="input-group">
                                <input className="form-control" id="privateKey"
                                                                aria-label="Private Key" type="number" />
                                <span className="input-group-btn"><button className="btn btn-secondary" id="randomButton"
                                                                    type="button">Random</button></span>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label" htmlFor="data">PublicKey</label>
                            <input className="form-control" id="publicKey" disabled="" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </>
  )
}

export default KeyPair
