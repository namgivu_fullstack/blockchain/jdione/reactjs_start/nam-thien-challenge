import TopNav from "./_TopNav"

const KeyPair = () => {
  return (
    <>
      <TopNav activeName='signature' />

      <div className="container">
        <div className="card">

          <div className="card-header"><h4>Signatures</h4>
            <ul className="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
              <li className="nav-item"><a className="nav-link active" id="sign-tab" data-toggle="tab" href="#sign" role="tab" aria-controls="sign" aria-selected="true">Sign</a></li>
              <li className="nav-item"><a className="nav-link" id="verify-tab" data-toggle="tab" href="#verify" role="tab" aria-controls="verify" aria-selected="false">Verify</a></li>
            </ul>
          </div>

          <div className="card-body" id="card">
            <div className="tab-content" id="myTabContent">

              <div className="tab-pane show active" id="sign" role="tabpanel" aria-labelledby="sign-tab">
                <form className="form-horizontal">
                  <div className="form-group"><label className="control-label" htmlFor="data">Message</label><textarea className="form-control" id="sign-message" rows="5" aria-label="Message"></textarea></div>
                  <div className="form-group"><label className="control-label" htmlFor="data">Private Key</label><input className="form-control" id="privateKey" type="number" /></div>
                  <div className="form-group">
                    <button className="btn btn-block btn-primary" id="sign-button" type="button">Sign</button>
                  </div>
                  <div className="form-group"><label className="control-label" htmlFor="data">Message Signature</label><input className="form-control" id="sign-signature" disabled="" /></div>
                </form>
              </div>

              <div className="tab-pane" id="verify" role="tabpanel" aria-labelledby="verify-tab">
                <form className="form-horizontal">
                  <div className="form-group"><label className="control-label" htmlFor="data">Message</label><textarea className="form-control" id="verify-message" rows="5" aria-label="Message"></textarea></div>
                  <div className="form-group"><label className="control-label" htmlFor="data">Public Key</label><input className="form-control" id="publicKey" /></div>
                  <div className="form-group"><label className="control-label" htmlFor="data">Signature</label><input className="form-control" id="verify-signature" /></div>
                  <div className="form-group">
                    <button className="btn btn-block btn-primary" id="verify-button" type="button">Verify</button>
                  </div>
                </form>
              </div>

            </div>
          </div>

        </div>
      </div>
    </>
  )
}

export default KeyPair
