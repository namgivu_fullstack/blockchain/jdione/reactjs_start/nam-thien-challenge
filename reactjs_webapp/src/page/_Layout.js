import { Outlet, Link } from "react-router-dom"

const _Layout = () => {
  return (
    <>
      <nav>
        <ul>
          <li> <Link to="/">Home</Link> </li>
          <li> <Link to="/blockchain/public-private-keys/keys">KeyPair</Link> </li>
        </ul>
      </nav>

      {/* page content will be displayed in :Outlet here */}
      <Outlet />
    </>
  )
}

export default _Layout
