
const TopNav = (props) => {
  let active = {
    Key         : props.activeName === 'key'         ? 'active' : '',
    Signature   : props.activeName === 'signature'   ? 'active' : '',
    Transaction : props.activeName === 'transaction' ? 'active' : '',
    Blockchain  : props.activeName === 'blockchain'  ? 'active' : '',
  }
  console.log(active)


  return (
    <>
      <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a className="navbar-brand" href="/">CLONED Blockchain Demo: Public / Private Keys &amp; Signing</a>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar"
                aria-controls="collapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="navbar-collapse collapse" id="collapsingNavbar">
          <ul className="navbar-nav ml-auto">
            <li className={`nav-item ${active.Key}`}         > <a className="nav-link" href="/blockchain/public-private-keys/keys">TODO Keys</a></li>
            <li className={`nav-item ${active.Signature}`}   > <a className="nav-link" href="/blockchain/public-private-keys/signatures">TODO Signatures</a></li>
            <li className={`nav-item ${active.Transaction}`} > <a className="nav-link" href="/blockchain/public-private-keys/transaction">TODO Transaction</a></li>
            <li className={`nav-item ${active.Blockchain}`}  > <a className="nav-link" href="/blockchain/public-private-keys/blockchain">TODO Blockchain</a></li>
          </ul>
        </div>
      </nav>
    </>
  )
}

export default TopNav
